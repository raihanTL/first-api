const mysql = require('mysql2');
const config = require('../config/default.json');

// console.log(config[0].MYSQL.HOST);
const pool = mysql.createPool({
    host: config[0].MYSQL.HOST,
    user: config[0].MYSQL.USER,
    database: config[0].MYSQL.DATABASE,
    password: config[0].MYSQL.PASSWORD
});

module.exports = pool.promise();