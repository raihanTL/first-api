const express = require('express');
const res = require('express/lib/response');
const db = require('../dbConnect/mysqlConnect.js');
const queries = require('./sqlQueries.js');

module.exports = class users{
    
    static async showInterns(){
        // let allInterns = await db.execute("SELECT * FROM interns;");
        let allInterns = await db.execute(queries.sqlQueries.get_all_users);
        return allInterns[0];
    }

    static async getInternByNid(id){
        try{
            // let user = await db.execute(`SELECT * FROM interns WHERE nid = ${id}`);
            let user = await db.execute(queries.sqlQueries.get_user_by_id, [id]);
            return user[0]; 
        }
        catch(err){
            console.log(err);
            return err;
        } 
    }


    static async addIntern(intern){
        try{
            // await db.execute(`INSERT INTO interns(name, role, email, nid) VALUES('${intern.name}', '${intern.role}', '${intern.email}', ${intern.nid});`);
            await db.execute(queries.sqlQueries.add_user, [intern.name, intern.role, intern.email, intern.nid]);
        }
        catch(err){
            console.log(err);
        }  
    }

    static async deleteIntern(nid){
        try{
            // await db.execute(`DELETE FROM interns WHERE nid = '${nid}';`);
            await db.execute(queries.sqlQueries.delete_user, [nid]);
            console.log("deletion was successful");
        }
        catch(err){
            console.log(err);
            return err;
        }
    }

    static async updateDetail(obj){
        try{
            await db.execute(queries.sqlQueries.update_detail, [obj.name, obj.role, obj.email, obj.nid]);
            console.log("update was successful");
        }catch(err){
            console.log(err);
        }
    }
}