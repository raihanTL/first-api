const sqlQueries = {
    'add_user': `INSERT INTO interns(name, role, email, nid) VALUES(?, ?, ?, ?);`,
    'delete_user': `DELETE FROM interns WHERE nid = ?;`,
    'get_user_by_id':`SELECT * FROM interns WHERE nid = ?;`,
    'get_all_users': `SELECT * FROM interns;`,
    'update_detail': `UPDATE interns SET name = ?, role = ?, email = ? WHERE nid = ?;`
}

module.exports = {
    sqlQueries
};