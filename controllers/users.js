const express = require('express');
const db = require('../dbConnect/mysqlConnect.js');
const usersModel = require('../models/users.js');

exports.getIntern = async (req, res) => {
    const { nid } = req.params;
    const foundUser = await usersModel.getInternByNid(nid);
    res.send(foundUser);
}

exports.showInterns = async (req, res) => {
    const allInterns = await usersModel.showInterns();
    res.json(allInterns);

};


const checkExistence = async (nid) => {
    const foundUser = await usersModel.getInternByNid(nid);

    if(foundUser.length == 0){
        return false;
    }
    else 
        return true;

}

exports.createIntern = async (req, res) => {
    const newIntern = req.body;
    let nid = newIntern.nid;
    const temp = await checkExistence(nid);

    if(!exists){
        try{
            await usersModel.addIntern(newIntern);
            res.send(`User ${newIntern.name} was added`);
        }catch(err){
            res.send(err);
        }
    }
    else{
        res.send(`User ${newIntern.name} already exists.`);
    }
};

exports.updateInternDetail = async (req, res) => {
    // console.log(req);
    const obj = req.body;
    try{
        await usersModel.updateDetail(obj);
        res.send("Update operation was successful");
    }catch(err){
        res.send(err);
    }
}

exports.removeIntern = async(req, res) => {
    const obj = req.body;
    let nid = obj.nid;
    const exists = await checkExistence(nid);

    if(exists){
        try{
            await usersModel.deleteIntern(nid);
            res.send(`User with nid ${nid} was removed successfully`);
        }
        catch(err){
            res.send(err);
        }
    }
    else{
        res.send(`User with nid ${nid} does not exist in the database`);
    }
    
}
 