const express = require('express');
const bodyParser = require('body-parser');

const usersRoutes = require('./routes/users.js');

const app = express();
app.use(bodyParser.json());

app.use('/users', usersRoutes);

app.get('/', (req, res) => {
    res.send("Hello from the server");
});

app.listen(3000, () =>{
    console.log("Listening in port 3000...");
});